import ccclass = cc._decorator.ccclass;
import executeInEditMode = cc._decorator.executeInEditMode;
import PolygonCollider = cc.PolygonCollider;
import property = cc._decorator.property;
import {LQCollide} from "./lq_collide";
import {LQCollideShape} from "../lq_base/data/lq_const";

@ccclass
@executeInEditMode
export class LQCollideBase extends cc.Component {
    @property({tooltip: '多边形自动同步cocos PolygonCollider 组件中的碰撞点'})
    protected auto_update_point: boolean = true;

    private find_collide() {
        const collide = this.node.getComponent(LQCollide);
        if (!collide) {
            console.error(this.node.name + ':没有找到LQCollide组件');
            return undefined;
        }
        return collide;
    }

    protected onLoad() {
        const collide = this.find_collide();
        if (!collide) {
            return;
        }
        collide.init_lq_collide();
    }

    protected onEnable() {
        const collide = this.find_collide();
        if (!collide) {
            return;
        }
        collide.enable_lq_collide();
    }

    protected onDisable() {
        const collide = this.find_collide();
        if (!collide) {
            return;
        }
        collide.disable_lq_collide();
    }

    protected onDestroy() {
        const collide = this.find_collide();
        if (!collide) {
            return;
        }
    }

    protected onFocusInEditor() {
        const collide = this.find_collide();
        if (!collide) {
            return;
        }
    }

    protected onLostFocusInEditor() {
        const collide = this.find_collide();
        if (!collide) {
            return;
        }
    }

    protected resetInEditor() {
        const collide = this.find_collide();
        if (!collide) {
            return;
        }
    }

    protected update(dt: number) {
        if (!CC_EDITOR || !this.auto_update_point) {
            return;
        }
        const collide = this.find_collide();
        if (!collide) {
            return;
        }
        if (collide.collide_shape !== LQCollideShape.Polygon) {
            return;
        }
        const polygonCollider = this.node.getComponent(PolygonCollider);
        if (!polygonCollider) {
            return;
        }
        collide.polygon_points = polygonCollider.points;
    }
}