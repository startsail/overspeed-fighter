import SingleBase from '../../base/SingleBase';
import PlatformApi from '../PlatformApi';
import {cUI} from '../../../config/Config';

export interface videoAdType {
    show(obj: { success(), fail() });

    onLoad();

    offLoad();

    load();

    onError(errorHandler: Function);

    offError();

    onClose(callback: Function);

    offClose(callback: Function);

    onReward(callback: Function);
}

export default class KwaiApi extends SingleBase implements PlatformApi {
    videoAd: videoAdType;
    callback: Function;
    recorder: any;
    videoPath: string;
    enabled: boolean = false;

    onLoad() {
        ks.init({"appId": "ks706502191964790040"});
        this.showShareMenu();
        this.enabled = ks.isSupport(ks.Support.features.RewardVideo);
        if(this.enabled) {
            this.loadVideoAd();
        }
        this.loadRecordVideo(1);
    }

    readyGo() {
        ks.readyGo();
    }

    showVideoAd(callback: Function, is_share: boolean = true) {
        if(!this.enabled) {
            this.showToast('当前版本无法显示视频', 'none');
            return;
        }
        this.callback = callback;
        this.videoAd.show({
            success() {

            },
            fail() {
                this.showToast('视频显示失败, 稍后重试', 'none');
            }
        });
    }

    reLoadVideo() {

    }

    showToast(title: string, icon: string = 'none', duration: number = 2000) {
        cUI.showTips(title,duration);
    }

    loadVideoAd(): void {
        this.videoAd = ks.createRewardedVideoAd({
            adUnitId: '2300000642_01',
        });
        if(!this.videoAd) {
            this.showToast('激励广告组件获取失败');
            return;
        }
        this.videoAd.onClose(() => {
            this.showToast('需要完整播放视频才能领取奖励哟！', 'none')
        });
        this.videoAd.onReward(() => {
            if(this.callback) {
                this.callback();
            }
            this.callback = null;
        });
        this.videoAd.onError((err) => {
            console.log('onError' + err);
            // this.showToast('视频加载错误', 'fail');
        });
    }

    onShareAppMessage(callback: Function, is_reward?: boolean): void {
        ks.shareToMsg({
            title: '分享标题',
            desc: '分享描述',
            iconUrl: '',
            imageUrl: '',
            response() {
                callback && callback();
                console.log("分享完成");
            }
        });
    }

    onShareAppVideo(callback: Function, is_reward?: boolean): void {
        let videoPath = this.videoPath;
        if(!videoPath) {
            this.onShareAppMessage(callback, is_reward);
            return;
        }
        let self = this;
        this.recorder.publishVideo({
            video: self.videoPath, // 指定需要发布的视频片段，不指定则默认发布最后一次 start , stop 之间生成的视频
            callback(error) {
                if(error) {
                    console.log(`发布录屏失败： ${JSON.stringify(error)}`)
                } else {
                    console.log("发布录屏成功")
                }
            }
        });
    }

    openAwemeUserProfile(callback: Function): void {

    }

    showShareMenu(): void {

    }

    startRecordVideo(duration: number): void {
        if(!this.recorder) {
            return;
        }
        this.recorder.start();
    }

    stopRecordVideo(): void {
        if(!this.recorder) {
            return;
        }
        this.recorder.stop();
    }

    isRecordVideo(): boolean {
        return !!this.videoPath;
    }

    setFrameRate(fps: number): void {

    }

    isPlatform(): boolean {
        return !!window.ks;
    }

    loadRecordVideo(res): void {
        this.recorder = ks.createMediaRecorder();
        if(!this.recorder) {
            console.log('当前版本不支持录屏');
            return;
        }
        this.recorder.onStart((res) => {
            console.log("录屏开始");
            // do something;
        });
        this.recorder.onStop((res) => {
            this.videoPath = res.videoID;
            console.log('录屏结束', res)
        });
        this.recorder.onPause(res => {
            console.log(res)
        });
        this.recorder.onResume(res => {
            console.log(res)
        });
        this.recorder.onError(res => {
            console.log(res)
        });
    }

    reportAnalytics(name: string, data: any = {}) {
    }

    createInterstitialAd() {
    }

    showInterstitialAd() {
    }
}