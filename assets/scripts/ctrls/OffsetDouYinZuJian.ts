const {ccclass, property} = cc._decorator;

@ccclass
export default class OffsetDouYinZuJian extends cc.Component {

    @property
    offsetY: number = 0;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        if (window.wx && false) {
            let s = wx.getMenuButtonBoundingClientRect();
            let se = wx.getSystemInfoSync();
            this.node.y = cc.winSize.height / 2 - (s.height / 2 + s.top) * (cc.winSize.height / se.screenHeight)+ this.offsetY;
        } else if (window.tt) {
            let s = tt.getMenuButtonLayout();
            let se = tt.getSystemInfoSync();
            this.node.y = cc.winSize.height / 2 - (s.height / 2 + s.top) * (cc.winSize.height / se.screenHeight)+ this.offsetY;
        } else if (cc.sys.isBrowser) {
            this.node.y = this.node.y + this.offsetY;
        }
    }

    start () {

    }
}
