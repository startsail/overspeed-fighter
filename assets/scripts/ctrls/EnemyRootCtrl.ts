// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Utils from "../common/utils/Utils";
import { $, cNodePool, cUI, gameData } from "../config/Config";
import BossBase from "../items/enemys/BossBase";
import EnemyBase from "../items/enemys/EnemyBase";
import { gameScene } from "../view/scene/GameScene";

const { ccclass, property } = cc._decorator;

@ccclass
export default class EnemyRootCtrl extends cc.Component {

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {

    }

    addEnemy() {
        if(cc.director.getScheduler().isScheduled(this.addEnemyTimer, this)){
            return;
        };
        this.schedule(this.addEnemyTimer,0.05);
    }

    addEnemyTimer(){
        if(this.node.childrenCount >= (3+gameData._enemy_wave*3 + 0.4*(gameData._pre_wave_enemy_time)) || this.node.childrenCount > 150){
            this.unschedule(this.addEnemyTimer);
            return;
        }
        let enemyKeyArr = ["yuan_lv", "sanjiaoxing_fen", "fangkuai_huang", "wubianxing_zi", "liubianxing_lan", "sijiaoxing_hong"];
        let len = enemyKeyArr.length - 1;
        if (len > gameData._enemy_wave) len = gameData._enemy_wave;
        let index = Utils.getNumMinToMax(0, len);
        cNodePool.get($.Prefab.enemy + ("enemy_" + enemyKeyArr[index])).then(value => {
            // value.position = Utils.getRadomPos(cc.v2(), 1.9);
            value.getComponent(EnemyBase).initData(enemyKeyArr[index]);
            value.parent = this.node;
            // value.getComponent(bullet).shootToPos(endPos, 0.5);
            gameScene.GameUI.updateEnemy();
        })
    }

    addBoss(){
        let index = (Math.floor(gameData._enemy_wave/4 - 1)%5);

        let str = $.Prefab["boss_" + index]
        cNodePool.get(str).then(value => {
            // value.position = Utils.getRadomPos(cc.v2(), 1.9);
            value.getComponent(BossBase).initData();
            value.parent = this.node;
            // value.getComponent(bullet).shootToPos(endPos, 0.5);
            gameScene.GameUI.updateEnemy();
            
        })
        gameData._kill_enemy_time = gameData._kill_enemy_time+6000;
    }

    checkEnemyCount() {
        // console.log('-------------checkEnemyCount:', this.node.childrenCount);
        if (this.node.childrenCount <= 0 && (gameData._pre_wave_start_time + gameData._pre_wave_enemy_time) - gameData._game_scoend < 2.5) {
            gameScene.maskNode.active = true;
            this.scheduleOnce(() => {
                if (this.node.childrenCount > 0) {
                    gameScene.maskNode.active = false;
                    return;
                }
                //获取掉落星星
                gameScene.PropRoot.itemsMovePlayer();
                // cUI.addLayer($.Prefab.shopNode);
                // gameScene.updateBuKuang();
            }, 0.5)
            // gameData._pre_wave_start_time + gameData._pre_wave_enemy_time
        }
    }

    updateEnemyData() {
        gameData._pre_wave_start_time = gameData._game_scoend + gameData._pre_wave_enemy_time;
        gameData._enemy_wave++;
        gameData._pre_wave_min_enemy = 1 + Math.floor(gameData._enemy_wave / 2);
        gameData._pre_wave_max_enemy = 2 + Math.floor(gameData._enemy_wave);
        gameData._pre_wave_enemy_time = 9 + gameData._enemy_wave*2;
        if (gameData._pre_wave_enemy_time > 60) gameData._pre_wave_enemy_time = 61;
        if (gameData._pre_wave_min_enemy > 10) gameData._pre_wave_min_enemy = 10;
        if (gameData._pre_wave_max_enemy > 20) gameData._pre_wave_max_enemy = 20;
        gameData._kill_enemy_time = 120000 + gameData._enemy_wave * 5000;
        gameScene.GameUI.updateWave();
        if(gameData._enemy_wave > 0 && gameData._enemy_wave % 4 == 0)this.addBoss();
    }

    getCloserEnemyPos(pos: cc.Vec2) {
        let nearestPos: cc.Vec2 = undefined;
        if (this.node.childrenCount <= 0) return undefined;
        let count = this.node.childrenCount;
        let nextPos: cc.Vec2;
        let nearestDis: number = 1000000000;
        for (let i = 0; i < count; i++) {
            nextPos = this.node.children[i].position;
            let nextDis = Utils.getDisTwoPos(nextPos, pos, true);
            if (nextDis < nearestDis) {
                nearestDis = nextDis;
                nearestPos = this.node.children[i].position;
            }
        }
        return nearestPos;
    }

    // update (dt) {}
}
