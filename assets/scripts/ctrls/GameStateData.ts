// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import SingleBase from "../common/base/SingleBase";
import { p_data_mager } from "../Mangers/playerDataMager/PlayerDataMager";

const {ccclass, property} = cc._decorator;

@ccclass
export default class GameStateData extends SingleBase {
   /**是否已经调用过授权 */
   shouQuan: boolean = false;

   /**上一局游戏生存的周期数 */
   _last_game_day: number = 0;
   /**是否强力开局 */
   _is_strong_start: boolean = false;
   /**游戏进行的毫秒数 */
   _game_milliscoend: number = 0;
   /**游戏进行的秒数 */
   _game_scoend: number = 0;
   /**第几波敌人 */
   _enemy_wave: number = 0;
   /**当前波次开始出敌时间 */
   _pre_wave_start_time: number = 0;
   /**当前波次持续出敌时间 */
   _pre_wave_enemy_time: number = 10;
   /**当前波次每秒最低出敌数 */
   _pre_wave_min_enemy: number = 1;
   /**当前波次每秒最高出敌数 */
   _pre_wave_max_enemy: number = 2;
   /**本局游戏击杀敌人总数 */
   _kill_enemy_count: number = 0;
   /**当前获得星光数 */
   _xing_guang_count: number = 0;
   /**消灭所有敌人的限制时间 */
   _kill_enemy_time: number = 120000;
   /**是否开启全自动射击 */
   _auto_shoot: boolean = false;

   initData(){
      this._game_scoend = 0;
      this._game_milliscoend = 0;
      this._enemy_wave = 0;
      this._pre_wave_start_time = 0;
      this._pre_wave_enemy_time =7;
      this._pre_wave_min_enemy = 1;
      this._pre_wave_max_enemy = 2;
      this._kill_enemy_count = 0;
      this._xing_guang_count = 0;
      this._kill_enemy_time = 120000;
   }

   updatePlayerData(){
      p_data_mager.playerData.killRivalCount += this._kill_enemy_count;
      if(this._enemy_wave > p_data_mager.playerData.maxLifeDay)p_data_mager.playerData.maxLifeDay = this._enemy_wave;
      if(this._kill_enemy_count > p_data_mager.playerData.maxKillCount)p_data_mager.playerData.maxKillCount = this._kill_enemy_count;
      p_data_mager.saveData();
   }

}
