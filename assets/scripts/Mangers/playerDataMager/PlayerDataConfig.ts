
/**服务器存储记录字段 */
export interface ServerData {
    openId: string,//玩家玩家唯一标识
    nickName: string,//玩家昵称
    avatarUrl: string,//玩家头像路径
    killRivalCount: number,//击杀敌人总数
    maxLifeDay: number,//单局游戏最大存活周期
    maxKillCount: number,//单局游戏最高击杀数
    attLv: Array<number>,//强化属性的等级
    xinPianSuiPian: number,//芯片碎片
}

/**玩家数据结构 */
export interface PlayerData extends ServerData {
    /**玩家在房间内的id */
    playerId: string,
    rank: string,//段位
    /** 本回合剩余行动时间 */
    actionTime: number,
    isFollowed: boolean,
    base_lucky: number,
    base_HP: number,
    base_speed: number,
    base_bullet_cd: number,
    base_bullet_speed: number,
    base_penetrate_count: number,
    base_volume: number,
    base_shoot_count: number,
    base_bullet_volume: number,
    base_shoot_range: number,
    base_frozen: number,
    base_life_stealing: number,
    base_hurt: number
}
