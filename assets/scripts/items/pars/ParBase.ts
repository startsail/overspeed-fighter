import { cNodePool } from "../../config/Config";

const {ccclass, property} = cc._decorator;

@ccclass
export default class ParBase extends cc.Component {

    @property(cc.ParticleSystem)
    particle: cc.ParticleSystem = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }

    protected onEnable(): void {
        this.particle.resetSystem();
        this.scheduleOnce(()=>{
            cNodePool.put(this.node);
        }, 1.5)
    }

    stopPar(){
        this.particle.stopSystem();
    }

    clearPar(){
        this.particle.resetSystem();
        this.particle.stopSystem();
    }

    resetPar(){
        this.particle.resetSystem();
    }

    // update (dt) {}
}
