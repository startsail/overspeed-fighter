// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Utils from "../../common/utils/Utils";
import { $, cNodePool, cSound } from "../../config/Config";
import { gameScene } from "../../view/scene/GameScene";
import EnemyBase from "../enemys/EnemyBase";
import SkillBase from "./SkillBase";

const {ccclass, property} = cc._decorator;

@ccclass
export default class skill_dianci_zd extends SkillBase{

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }

    startAnim(){
        cSound.playEffect($.Sound["shandian"+2]);
        this.is_enable = false;
        this._hurt = 5;
        this.node.getComponent(cc.Animation).play();
    }

    finish(){
        cNodePool.put(this.node);
    }

    hit(){
        gameScene.EnemyRoot.node.children.forEach(child =>{
            if(child.getComponent(EnemyBase))child.getComponent(EnemyBase).dianCiJingMo(gameScene.heroCtrl.skill_ctrl.skillZdMap[this.node.name].duration_time);
        })
    }

    // update (dt) {}
}
