// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { $, cUI } from "../../config/Config";
import { gameScene } from "../../view/scene/GameScene";
import { SkillList } from "./SkillList";

const { ccclass, property } = cc._decorator;

@ccclass
export default class zd_skill_ctrl extends cc.Component {

    @property(cc.Sprite)
    timer: cc.Sprite = null;
    @property(cc.Sprite)
    icon: cc.Sprite = null;

    //技能基础属性
    time_bullet: number = 150;
    bullet_count: number = 1;
    duration_time: number = 1000;
    time_cd: number = 3000;

    _timer_flag: number = 0;
    _cd_timer: number = -1;
    _skill_key: string = "";

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {

    }

    initData(key: string) {
        if(this._skill_key == ""){
            this.icon.spriteFrame = cUI.getTextureSync($.Texture.shopItems + key);
        }
        this._skill_key = key;
        this.time_bullet = gameScene.heroCtrl.skill_ctrl.skillZdMap[key].time_bullet;
        this.bullet_count = gameScene.heroCtrl.skill_ctrl.skillZdMap[key].bullet_count;
        this.duration_time = gameScene.heroCtrl.skill_ctrl.skillZdMap[key].duration_time;
        this.time_cd = gameScene.heroCtrl.skill_ctrl.skillZdMap[key].time_cd;
    }

    clickFunc(){
        if(this._timer_flag > 0){
            return;
        }
        SkillList[this._skill_key](this._skill_key, this.time_bullet, this.bullet_count, this.duration_time);
        this.startTimer();
    }

    startTimer() {
        this._timer_flag = this.time_cd;
        this.timer.fillRange = 1;
        this._cd_timer = setInterval(this.timerFunc.bind(this), 50);
    }

    stopTimer() {
        clearInterval(this._cd_timer);
    }

    protected onDestroy(): void {
        clearInterval(this._cd_timer);
    }


    timerFunc() {
        if (gameScene.is_pause) return;
        if (this._timer_flag > 0) {
            this._timer_flag -= 50
            this.timer.fillRange = this._timer_flag / this.time_cd;
        } else {
            this.stopTimer();
        }
    }
}
