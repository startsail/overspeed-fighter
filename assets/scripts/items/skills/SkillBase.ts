// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { LQCollide } from "../../../lq_collide_system/lq_collide";

const {ccclass, property} = cc._decorator;

@ccclass
export default class SkillBase extends LQCollide {

    _key: string = "";
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }

    startSkill(pos:cc.Vec2, key: string): void {
        this.node.position = pos;
        this._key = key;
    }


    initData(){

    }

    // @ts-ignore
    public on_collide(collide: LQCollide): void {
    }

    //@ts-ignore
    public on_enter(collide: LQCollide) {
    }

    //@ts-ignore
    public on_exit(collide: LQCollide) {
    }

   

    // update (dt) {}
}
