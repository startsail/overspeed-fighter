// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Utils from "../../common/utils/Utils";
import { $, cNodePool, cSound } from "../../config/Config";
import SkillBase from "./SkillBase";

const {ccclass, property} = cc._decorator;

@ccclass
export default class skill_shenlei extends SkillBase{

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }

    startAnim(){
        let index = Utils.getNumMinToMax(0,2);
        cSound.playEffect($.Sound["shandian"+index]);
        if(this.node.y > 0)this.node.y = 0;
        this.is_enable = false;
        this._hurt = 5;
        this.node.getComponent(cc.Animation).play();
    }
    
    hit(){
        this.is_enable = true;
    }

    finish(){
        cNodePool.put(this.node);
    }

    // update (dt) {}
}
