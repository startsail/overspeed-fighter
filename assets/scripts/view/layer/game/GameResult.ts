
import { $, cSound, cUI, gameData, pfApi } from '../../../config/Config';
import ViewBase from '../../base/ViewBase';
import { gameScene } from '../../scene/GameScene';

const {ccclass, property} = cc._decorator;

@ccclass
export default class GameResult extends ViewBase {

    @property(cc.Label)
    killLb: cc.Label = undefined;
    @property(cc.Label)
    lifeLb: cc.Label = undefined;

    protected onLoad(): void {
        this.killLb.string = "击毁敌人："+gameData._kill_enemy_count;
        this.lifeLb.string = "生存周期："+gameData._enemy_wave;
        gameData._last_game_day = gameData._enemy_wave;
        pfApi.showInterstitialAd();
        pfApi.stopRecordVideo();
    }


    onBtnExit(e?: cc.Event.EventTouch) {
        cSound.playEffect($.Sound.btn_0);
        cUI.removeLayer(this.node);
        cUI.addScene($.Prefab.main_scene);
    }

    onBtnShare(){
        cSound.playEffect($.Sound.btn_0);
        pfApi.onShareAppVideo(this.onBtnExit.bind(this));
    }

}