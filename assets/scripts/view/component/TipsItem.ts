import {TipsItemView} from '../base/ViewSet';
var TIPS_SHOW_TIME = 2;
var TIPS_EXIT_TIME = 1.2;
/*0:正常状态, 1,显示状态, 2:移出状态,3:销毁状态*/
enum ItemStatus {
    show,
    removing,
    dead
}
const {ccclass, property, menu} = cc._decorator;
@ccclass
@menu('component/TipsItem')
export default class TipsItem extends TipsItemView {
    @property({type: cc.Enum(ItemStatus)})
    status = ItemStatus.show;
    @property
    y = 0;
    //状态经历的时间
    @property
    elapsed = 0;

    //被其他TipsView替换时需要移动的总距离
    @property
    offsetY = 0;

    //移出时移动的距离
    @property
    removingY = 0;

    onLoad () {
        this.y = this.node.y;
    };

    onDestroy () {
        //console.log("tips view entity destroy");
    };

    setTipsText (text) {
        this._content.setLabel(text);
        setTimeout(() => {
            this._bgNode.width = this._content.width+60;
        }, 100);
    };

    setShow () {
        this.status = ItemStatus.show;
        this.elapsed = 0;
    };

    showExpired () {
        return this.status === ItemStatus.show && this.elapsed >= TIPS_SHOW_TIME;
    };

    setRemoving () {
        this.status = ItemStatus.removing;
        this.elapsed = 0;
    };

    setDead () {
        this.status = ItemStatus.dead;
        this.elapsed = 0;
    };

    dead () {
        return this.status === ItemStatus.dead;
    };

    getView() {
        return this.node;
    };

    updateOffsetY(offsetY) {
        this.offsetY = offsetY;
        this.node.y = this.y + this.offsetY + this.removingY;
    };

    addOffsetY(dy) {
        var offsetY = this.offsetY + dy;
        this.updateOffsetY(offsetY);
    };

    updateRemovingY(dy) {
        this.removingY = dy;
        this.node.y = this.y + this.offsetY + this.removingY;
    };
    // called every frame, uncomment this function to activate update callback
    update (dt) {
        switch(this.status)
        {
            case ItemStatus.show:
                if(this.elapsed >= TIPS_SHOW_TIME)
                {
                    return;
                }
                this.elapsed += dt;
                break;
            case ItemStatus.removing:
                if(this.elapsed >= TIPS_EXIT_TIME)
                {
                    this.setDead();
                    return;
                }
                this.elapsed += dt;
                this.elapsed = Math.min(this.elapsed, TIPS_EXIT_TIME);
                var opacity = 255 - this.elapsed * (255.0 / TIPS_EXIT_TIME);
                var dy = 200 * this.elapsed - 100 * Math.pow(this.elapsed, 2);
                this.updateRemovingY(dy);
                this.node.opacity = opacity;
                break;
            case ItemStatus.dead:
                break;
            default:
                break;
        }
    }
}