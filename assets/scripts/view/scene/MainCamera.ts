import { gameScene } from '../scene/GameScene';

const { ccclass, property } = cc._decorator;
export var mainCamera: MainCamera;
@ccclass
export default class MainCamera extends cc.Component {
    camera: cc.Camera;

    onLoad() {
        mainCamera = this;
        this.camera = this.node.$(cc.Camera);
    }

    protected start(): void {

    }

    setPos(pos: cc.Vec2) {
        this.node.position = pos;
    }

    setZoomRatio(ration: number){
        this.camera.zoomRatio = ration;
    }

    startGameAction() {
        cc.tween(this.camera)
            .to(0.5, { zoomRatio: 1 })
            .start();

        cc.tween(this.node)
            .to(0.5, { y: 0 })
            .start();
    }
}