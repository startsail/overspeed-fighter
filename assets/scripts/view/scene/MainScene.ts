import { MainSceneView } from '../base/ViewSet';
import { $, cNodePool, cSound, cStorage, cUI, gameData, pfApi } from '../../config/Config';
import Utils from '../../common/utils/Utils';
import UITools from '../../common/utils/UITools';
import { event_strong_start } from '../../config/ClientEvent';
import GameScene from './GameScene';
import { p_data_mager } from '../../Mangers/playerDataMager/PlayerDataMager';
import InfoNode from '../uiItems/InfoNode';

let is_loaded = false;
const { ccclass, property } = cc._decorator;
@ccclass
export default class MainScene extends MainSceneView {
    is_start: boolean = false;
    @property(InfoNode)
    infoNode: InfoNode = undefined;

    protected onLoad(): void {
        cUI.hideAllTips();
        cc.dynamicAtlasManager.reset();
        cNodePool.clearAll();
        this.initScene();
        this.initPlatform();
        setTimeout(() => {
            cSound.playMusic($.Sound.mainBgm)
        }, 1000);
    }



    protected start(): void {
        if (window.tt || window.wx) {
            this.shouQuan();
        }
    }

    initPlatform() {
    }

    initScene() {
        gameData._is_strong_start = false;
    }

    protected initEvents() {
        super.initEvents();
        this.registerEvent(event_strong_start, this.event_strong_start, this);
    }


    event_strong_start(e: event_strong_start) {
        this.startAction();
    }

    loadGameScene(res_url: string) {
        cUI.addScene($.Prefab.game_scene).then(value => {
            is_loaded = true;
            value.$(GameScene).initScene();
        });
    }


    loadGameRes() {
        let res_url = $.Texture.shopItems;
        cUI.createNode($.Prefab.loading).then(node => {
            node.y = 5;
            node.parent = cUI.getGameLoadingRoot();
            cUI.loadResDir(res_url, node).then(value => {
                node.destroy();
                this.loadGameScene(res_url);
            });
        })
    }

    startAction() {
        cSound.playEffect($.Sound.qifei);
        this.node.getChildByName("bg").active = false;
        let feiJi = this.node.getChildByName("player");
        feiJi.getChildByName("weiqi2").active = true;
        cc.tween(feiJi)
            .to(3, { y: cc.winSize.height * 0.7 }, { easing: t => t * t * t * t * t })
            .call(() => {
                feiJi.opacity = 0;
                this.loadGameRes();
            })
            .start();
    }

    onBtnStartGame(e?: cc.Event.EventTouch) {
        if (this.is_start) return;
        this.is_start = true
        cSound.playEffect($.Sound.btn_0);
        if (gameData._last_game_day > 0 && gameData._last_game_day < 5) {
            cUI.addLayer($.Prefab.strongStart);
        } else {
            this.startAction();
        }
        // return;
        // pfApi.reportAnalytics('enter_kaishiyouxi', {
        // });
    }

    onBtnAttNode(e?: cc.Event.EventTouch) {
        if (this.is_start) return;
        cSound.playEffect($.Sound.btn_0);
        cUI.addLayer($.Prefab.attNode);
    }

    onBtnNotice(e?: cc.Event.EventTouch) {
        cUI.addLayer($.Prefab.notice, false);
    }

    shouQuan() {
        if (gameData.shouQuan) return;
        gameData.shouQuan = true;
        let self = this;
        if (window.wx && false) {
            wx.getSetting({
                success(res) {
                  if (!res.authSetting['scope.userInfo']) {
                    wx.authorize({
                      scope: 'scope.userInfo',
                      success () {
                        // 用户已经同意小程序使用录音功能，后续调用 wx.startRecord 接口不会弹窗询问
                        wx.getUserInfo({
                            success: function (res) {
                                //   var userInfo = res.userInfo
                                //   var nickName = userInfo.nickName
                                //   var avatarUrl = userInfo.avatarUrl
                                //   var gender = userInfo.gender //性别 0：未知、1：男、2：女
                                //   var province = userInfo.province
                                //   var city = userInfo.city
                                //   var country = userInfo.country;
                                console.log(`getUserInfo 调用成功`, res.userInfo);
                                p_data_mager.playerData.avatarUrl = res.userInfo.avatarUrl;
                                p_data_mager.playerData.nickName = res.userInfo.nickName;
                                self.infoNode.initData();
                            },
                            fail(res) {
                                console.log(`getUserInfo 调用失败`, res.errMsg);
                            }
                        })
                      }
                    })
                  }
                }
              })
        } else {
            tt.login({
                success(_res) {
                    console.log("登录成功");
                    self.getUserInfo();
                },
            });
        }
        // this.node.getChildByName("btnShouQuan").destroy();
    }

    getUserInfo() {
        let self = this;
        // 获取用户信息
        tt.getUserInfo({
            // withCredentials: true,
            // withRealNameAuthenticationInfo: true,
            success(res) {
                console.log(`getUserInfo 调用成功`, res.userInfo);
                p_data_mager.playerData.avatarUrl = res.userInfo.avatarUrl;
                p_data_mager.playerData.nickName = res.userInfo.nickName;
                self.infoNode.initData();
            },
            fail(res) {
                console.log(`getUserInfo 调用失败`, res.errMsg);
                tt.openSetting({
                    success: (res) => {
                        console.log("openSetting success");

                    },
                    fail: (err) => {
                        console.log("openSetting fail");

                    },
                    complete: (res) => {
                        console.log("openSetting complete");
                        tt.getUserInfo({
                            // withCredentials: true,
                            // withRealNameAuthenticationInfo: true,
                            success(res) {
                                console.log(`getUserInfo 调用成功`, res.userInfo);
                                p_data_mager.playerData.avatarUrl = res.userInfo.avatarUrl;
                                p_data_mager.playerData.nickName = res.userInfo.nickName;
                                self.infoNode.initData();
                            },
                            fail(res) {
                            },
                        });
                    },
                });
            },
        });
    }
}