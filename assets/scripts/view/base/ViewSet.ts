import ViewBase from './ViewBase';
export abstract class LoadingView extends ViewBase {
   
	__preload () {
		this.loadConfig();

    }
}
export abstract class NetLoadingView extends ViewBase {
	_tips_img: cc.Node;
	_tips_title: cc.Node;
   
	__preload () {
		this.loadConfig();
		this._tips_img = this.node.$('_tips_img');
		this._tips_title = this.node.$('_tips_title');

    }
}
export abstract class TipsItemView extends ViewBase {
	_content: cc.Node;
	_bgNode: cc.Node;
   
	__preload () {
		this.loadConfig();
		this._content = this.node.$('tips_bg/_content');
		this._bgNode = this.node.$('_bgNode');

    }
}
export abstract class NoticeView extends ViewBase {
	_bg: cc.Node;
	_btn_close: cc.Node;
	abstract onBtnClose(e?: cc.Event.EventTouch);
   
	__preload () {
		this.loadConfig();
		this._bg = this.node.$('_bg');
		this._btn_close = this.node.$('_bg/_btn_close');
		this._btn_close.click(this.onBtnClose.bind(this));

    }
}
export abstract class PauseView extends ViewBase {
   
	__preload () {
		this.loadConfig();

    }
}

export abstract class GameSceneView extends ViewBase {
	_btn_pause: cc.Node;
	abstract onBtnPause(e?: cc.Event.EventTouch);
   
	__preload () {
		this.loadConfig();
	
		this._btn_pause = this.node.$('UI/_btn_pause');
		this._btn_pause.click(this.onBtnPause.bind(this));
    }
}

export abstract class EnemyPlayerView extends ViewBase {
	// _wordBg: cc.Node;
	// _wordLabel: cc.Node;
	__preload () {
		this.loadConfig();
		// this._wordBg = this.node.$('wordBg');
		// this._wordLabel = this.node.$('wordBg/wordLabel');
	}
};

export abstract class CarItemView extends ViewBase {
	_bg: cc.Node;
	beforSpr: cc.Node;
	_sprNode_0: cc.Node;
	_sprNode_1: cc.Node;
	_sprNode_2: cc.Node;
	_sprNode_3: cc.Node;
	_sprNode_4: cc.Node;
	_sprNode_5: cc.Node;
	_sprNode_6: cc.Node;
	_sprNode_7: cc.Node;

	abstract onBtnSprNode(index: number);
   
	__preload () {
		this.loadConfig();
		this._bg = this.node.$('_bg');
		this.beforSpr = this.node.$("beforSpr");
		this._sprNode_0 = this.node.$('sprNode_0');
		this._sprNode_1 = this.node.$('sprNode_1');
		this._sprNode_2 = this.node.$('sprNode_2');
		this._sprNode_3 = this.node.$('sprNode_3');
		this._sprNode_4 = this.node.$('sprNode_4');
		this._sprNode_5 = this.node.$('sprNode_5');
		this._sprNode_6 = this.node.$('sprNode_6');
		this._sprNode_7 = this.node.$('sprNode_7');

		this._sprNode_0.click(this.onBtnSprNode.bind(this,0));
		this._sprNode_1.click(this.onBtnSprNode.bind(this,1));
		this._sprNode_2.click(this.onBtnSprNode.bind(this,2));
		this._sprNode_3.click(this.onBtnSprNode.bind(this,3));
		this._sprNode_4.click(this.onBtnSprNode.bind(this,4));
		this._sprNode_5.click(this.onBtnSprNode.bind(this,5));
		this._sprNode_6.click(this.onBtnSprNode.bind(this,6));
		this._sprNode_7.click(this.onBtnSprNode.bind(this,7));
    }
}

export abstract class MainSceneView extends ViewBase {
	__preload () {
		this.loadConfig();
    }
}