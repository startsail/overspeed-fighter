// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { HttpRequest } from "../../common/utils/HttpRequest";
import { $, cSound, cUI } from "../../config/Config";
import { p_data_mager } from "../../Mangers/playerDataMager/PlayerDataMager";

const {ccclass, property} = cc._decorator;

@ccclass
export default class InfoNode extends cc.Component {

    @property(cc.Label)
    nameLb: cc.Label = null;
    @property(cc.Label)
    rankLb: cc.Label = null;
    @property(cc.Sprite)
    avatarSpr: cc.Sprite = null;
    @property(cc.Sprite)
    rankIcon: cc.Sprite = undefined;
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        p_data_mager.playerData.rank = p_data_mager.getRankName();
        this.rankLb.string = p_data_mager.playerData.rank;
        this.nameLb.string = p_data_mager.playerData.nickName;
        HttpRequest.updateHttpSprite(p_data_mager.playerData.avatarUrl, this.avatarSpr);
        cUI.getTexture($.Texture.rankIcon + p_data_mager.getRankIndex()).then(value =>{
            this.rankIcon.spriteFrame = value;
        })
    }

    initData(){
        this.nameLb.string = p_data_mager.playerData.nickName;
        HttpRequest.updateHttpSprite(p_data_mager.playerData.avatarUrl, this.avatarSpr);
    }

    start () {

    }

    onBtnPlayerInfo(e?: cc.Event.EventTouch) {
        cSound.playEffect($.Sound.btn_0);
        cUI.addLayer($.Prefab.playerInfo);
    }

    // update (dt) {}
}
