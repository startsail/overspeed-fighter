import {cEvent} from './Config';

let keys: string[];
export class event_base {
    constructor(){
        
    }
    getName() {
        keys = keys || Object.keys(exports).slice(1);
        let name = keys.find(key => {
            return this instanceof exports[key];
        });
        if (name) {
            return name;
        }
        console.warn('not found key');
    }
    name: string = this.constructor.name;
    success?: boolean = true;
    msg_index?: number;
    static emit() {
        let event = new this();
        cEvent.emitEvent(event);
    }
    static emitIndex(index: number) {
        let event = new this();
        event['index'] = index;
        cEvent.emitEvent(event);
    }
    static emitObj(data: {isBingo: true, isPlayer: false}) {
        let event = new this();
        event['data'] = data;
        cEvent.emitEvent(event);
    }
}

/*恢复游戏*/
export class event_resume_game extends event_base {
    static getName(){return ""}

}

/*看广告*/
export class event_finish_ads extends event_base {
    static getName(){return ""}
    times: number;
}

/*强力开局选择*/
export class event_strong_start extends event_base {
    static getName(){return ""}
    index: number;
}
